#!/bin/bash
echo "RUNNING: ansible-playbook -i inventory devvmsetup.yml --ask-become-pass" 
echo "NOTE: Enter your password when prompted for BECOME password. It is used to sudo perform tasks."
#Actual run
ansible-playbook -i inventory devvmsetup.yml --ask-become-pass

#Dry run
#ansible-playbook -i inventory devvmsetup.yml --ask-become-pass --check
