# VirtualBox Development VM Setup

This project configures a Fedora or Ubuntu virtual machine (VM) using Ansible. It sets up essential configurations like DNS, proxy settings, and certificates, and installs common development tools such as:

* Docker
* Docker Compose
* Visual Studio Code
* VirtualBox Guest Additions

This automated setup streamlines the process of creating a consistent development environment within VirtualBox.


**Tested on:**  
Fedora 35  
Ubuntu 20.04 lts    
Ubuntu 21.10  
Ubuntu 22.04 lts


## Install virtualbox
1. Install virtualbox from [Here](https://www.virtualbox.org/wiki/Downloads) 

## Download Fedora OR Ubuntu iso
1. Download Fedora ISO from [here](https://getfedora.org/en/workstation/download/)
2. Download Ubuntu ISO from [here](https://ubuntu.com/download/desktop)

## Create virtualbox vm and install Fedora
1. Create virtualbox machine 
2. 3 cpu  4096MB ram is usable.  would recommend a 4GB swapfile with low ram so systemd-oomd doesnt start killing apps.
3. General-> Advanced -> Shared clipboard and dragndrop set to bidirectional
4. Use NAT or NAT network.  If you use NAT network you can define the network in virtualbox. Bridged if you know what you are doing.
6. Check enable 3d acceleration
7. Install Fedora as normal 

## IF REQUIRED - Install guest additions so we can copy and paste. 
1. Usually not required for fedora as guest additions come with pre installed.
2. For Ubuntu I usually recommend do apt update && apt upgrade, reboot, install guest additions from iso.
2. In virtualbox window -> devices -> insert guest addtions
3. Click RUN and enter password
4. Manually run if it doesnt prompt
```
cd /media/$USER/VBox_GAs_*
sudo bash ./VBoxLinuxAdditions.run

#If you have issues you may need to install kernel-headers

```
5. Reboot if required

## Install git and ansible
```
sudo apt update
sudo apt -y install git ansible
```

## Clone repo and run Ansible playbook
1. Clone this repo
```
#via https since the repo is public
git clone https://gitlab.com/keeneproject/ansible/ansible-dev-vm-setup.git
```

2. Run the playbook
```
cd ansible-dev-vm-setup
./runplaybook.sh
```
3. Reboot the machine for guest addition and group changes to take effect
5. It is safe to re-run multiple times to try to resolve errors.

## Optional steps to run before running the Ansible playbook - click to expand
<details><summary><i>Configure proxy for apt (ansible can fix later)</i></summary>  

You can uncomment and modify the proxy section in devvmsetup.yml if you want ansible to configure dns for you

1. Log into machine
2. Configure proxy
```
sudo tee -a /etc/environment <<EOT
#Environment proxy
HTTPS_PROXY=http://www.proxy.com:port
HTTP_PROXY=http://www.proxy.com:port
#Proxy for apt cli
https_proxy=http://www.proxy.com:port
http_proxy=http://www.proxy.com:port
EOT

#Add proxy to /etc/etc/dnf/dnf.conf for Fedora
```
</details>

<details><summary><i>Configure dns (ansible can fix later)</i></summary>

You can uncomment and modify the dns section in devvmsetup.yml if you want ansible to configure dns for you
1. Check if dns is working
```
nslookup www.google.com
curl https://www.google.com

#If working skip this section
#If not working proceed to step 2
```

2. Via the gui
```
a. Click top right notifications
b. Wired connected or network name -> wired settings
c. Click cog next to name
d. Ipv4 -> uncheck automatic
e. Enter in the dns field 8.8.8.8, 8.8.4.4
f. Apply
g. Check if dns is working again
h. Reboot cause networkmanager sucks
```

3. If you dont have gui access
```
#Mandatory to delete this as its a symlink.
sudo rm /etc/resolv.conf

# Add DNS to /etc/resolv.conf
sudo tee /etc/resolv.conf <<EOT
nameserver 8.8.8.8
nameserver 8.8.4.4
EOT

sudo vi /etc/NetworkManager/NetworkManager.conf 
insert dns=none in the [Main] Section

reboot #Because networkmanager sucks


```
</details>  

<details><summary><i>Setup ssh keys and add to gitlab</i></summary>   

If you want to use git via ssh instead of https
1.  To setup a new key
```
ssh-keygen
Enter for all defaults unless you want a passphrase
Go to next step to add the public key into GitLab
```
2. Alternatively: If you have previously saved ssh keys for use:
```
First run the following in the VM:
mkdir ~/.ssh
touch ~/.ssh/id_rsa
chmod 600 ~/.ssh/id_rsa
touch ~/.ssh/id_rsa.pub
chmod 644 ~/.ssh/id_rsa.pub
copy in contents for id_rsa/.pub
```

**Add public key to gitlab - or skip and use https with username and password**
1. Open your new Public key file (cat ~/.ssh/id_rsa.pub) and copy contents
2. Log into your GitLab account
3. Click on your profile picture in the upper right and select Preferences
4. In the left pane select SSH Keys
5. Paste the public key contents in the large text box
6. The title should auto populate from the key comment and expiration date is optional
7. Click Add key
</details>

<details><summary><i>Configure git after installing it</i></summary>  

1. Configure git
```
git config --global user.name "Your Name"
git config --global user.email "your email address"
```
2. You can then git clone via ssh and then push etc.

</details>


## notes
1. You could alternatively get ssh working to the guest and run the ansible playbook via ssh. Check the inventory file for example
2. You could use something like packer to configure a ubuntu image that is installed and has the base config and potentially public ssh keys
3. You could then use something like terraform or vagrant to install your packer box into virtualbox and execute the ansible playbook
4. If you have issues with guest additions insert the iso from virtualbox and run it manually.

## Other ansible commands
```
#Dry run a playbook
ansible-playbook -i inventory devvmsetup.yml --ask-become-pass --check

#Run a playbook - this is what is in runplaybook.sh
ansible-playbook -i inventory devvmsetup.yml --ask-become-pass

#Adhoc against custom inventory
ansible all -m ansible.builtin.setup -i inventory  #this gets facts about the inventory
ansible all -a 'ls -l ~/' -i inventory
ansible mymachine -m ping -i inventory

#Limit by groups or hostnames
ansible-playbook -i inventory devvmsetup.yml --ask-become-pass --limit local_machine
ansible-playbook -i inventory devvmsetup.yml --ask-become-pass --limit mymachine

#Prompt for ssh password (if you didnt have ssh-keys setup)
ansible-playbook -i inventory devvmsetup.yml --ask-pass --ask-become-pass

#Initialize a role
From main dir cd into roles and run:
ansible-galaxy init rolename
```

## Ansible Documentation
https://docs.ansible.com/ansible/latest/user_guide/index.html  
https://docs.ansible.com/ansible/2.8/user_guide/playbooks_best_practices.html#directory-layout
