#!/bin/bash

# Remove users .mozilla/firefox directory just to be safe
rm -rf /home/$(whoami)/.mozilla/firefox

# Start firefox in headless to ensure user has a profile generated
/usr/bin/firefox --headless > /dev/null 2>&1 & 

# Wait and then kill firefox
sleep 5
pkill -f firefox || true

# Run cert util to load the certs into the firefox cert db
# Check the correct path if you enable on fedora
databasedir=$(find /home/$(whoami)/.mozilla/firefox -name cert9.db -printf '%h\n')
/usr/bin/certutil -A -n cert1 -t TC,c -i /usr/local/share/ca-certificates/cert1.pem -d sql:"$databasedir"
/usr/bin/certutil -A -n cert2 -t TC,c -i /usr/local/share/ca-certificates/cert2.pem -d sql:"$databasedir"


